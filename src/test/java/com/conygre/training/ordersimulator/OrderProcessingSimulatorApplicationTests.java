package com.conygre.training.ordersimulator;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("h2")
@SpringBootTest
class OrderProcessingSimulatorApplicationTests {

	@Test
	void contextLoads() {
	}

}
