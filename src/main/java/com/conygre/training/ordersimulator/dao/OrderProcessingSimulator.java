package com.conygre.training.ordersimulator.dao;

import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import com.conygre.training.ordersimulator.entities.Trade;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.socket.client.IO;
import io.socket.client.Socket;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;


/**
 * This class is a test harness class - it only exists for testing.
 * 
 * This is SIMULATING sending trade orders to an exchange for them to be filled.
 * 
 * This class uses the following codes to represent the state of an order:
 * 
 * 0 : initialized i.e. has not yet been processed at all
 * 1 : processing  i.e. the order has been sent to an exchange, we are waiting for a response
 * 2 : filled i.e. the order was successfully placed
 * 3 : rejected i.e. the order was not accepted by the trading exchange
 * 
 * The above are JUST SUGGESTED VALUES, you can change or improve as you see think is appropriate.
 */
@Repository
public class OrderProcessingSimulator {

	private Socket socket;
	private ObjectWriter ow;

	public OrderProcessingSimulator(){
		ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		IO.Options opts = new IO.Options();
		opts.forceNew = true;
		opts.reconnection = false;
		this.socket = IO.socket(URI.create("ws://socketserver-socketserver.emeadocker48.conygre.com"),opts);
		this.socket.connect();
		LOG.debug("Socket has been initialized");
	}
	
	// Standard mechanism for logging with spring boot - org.slf4j is built-in to spring boot
	private static final Logger LOG = LoggerFactory.getLogger(OrderProcessingSimulator.class);

	// You'll need to change these to match whatever you called your table and status_code field
	// You may also need to change the database name in application-mysql.properties and application-h2.properties
	// The default database name that's used here is "appDB"
	@Value("${mysql.table.name}")
	private String TABLE;
	
	@Value("${mysql.statuscode.colname}")
	private String STATUS_CODE;

	@Value("${percent.failures}")
	private int percentFailures = 10;

	@Value("${TRADE_TABLE:trade_activity}")
	private String TRADE_TABLE;

	@Autowired
	private JdbcTemplate template;

	/**
	 * Any record in the configured database table with STATE=0 (init)
	 * 
	 * Will be changed to STATE=1 (processing)
	 * 
	 * Note the @Scheduled : this tells spring to automatically call this method on a schedule
	 * 
	 * 10000 indicates to spring that this method should be called every 10,000 milliseconds (10s)
	 */
	@Scheduled(fixedRateString = "${proc.rate.ms:10000}")
	public int findTradesForProcessing() {

		String sql_select_future_update = "SELECT id from " + TABLE + " WHERE " + STATUS_CODE + "='I'";
		List<Long> orderListPending = template.query(sql_select_future_update, (resultSet, i) -> resultSet.getLong(1));
		if(orderListPending.size() != 0){
			String asList = orderListPending.stream().map(String::valueOf).collect(Collectors.joining(","));
			try{
				this.socket.emit("order", asList);
				LOG.debug("Sending order list via socket: " + asList);
			}catch(Exception e){
				LOG.error(e.getMessage());
			}
			String sql = "UPDATE " + TABLE + " SET " + STATUS_CODE + "='P' WHERE id in (" +  asList + ")";
			LOG.debug("Order sql is: " + sql);
			int numberChanged = template.update(sql);
			LOG.debug("Updated [" + numberChanged + "] order from initialized (I) TO processing (P)");
			return numberChanged;
		}else{
			LOG.debug("Updated [0] order from initialized (I) TO processing (P)");
			return 0;
		}
	}
	
	/**
	 * Anything in the configured database table with STATE=1 (processing)
	 * 
	 * Will be changed to STATE=2 (filled) OR STATE=3 (rejected)
	 * 
	 * This method uses a random number to determine when trades are rejected.
	 * 
	 * Note the @Scheduled : this tells spring to automatically call this method on a schedule
	 * 
	 * 10000 indicates to spring that this method should be called every 10,000 milliseconds (10s)
	 */
	@Scheduled(fixedRateString = "${fill.rate.ms:15000}")
	public int findTradesForFillingOrRejecting() {
		int totalChanged = 0;
		int lastChanged = 0;

		do {
			lastChanged = 0;
			
			// use a random number to decide if we'll simulate success OR failure
			int randomInteger = new Random().nextInt(100);

			LOG.debug("Random number is [" + randomInteger +
					  "] , failure rate is [" + percentFailures + "]");
						
			if(randomInteger > percentFailures) {
				// Mark this one as success
				lastChanged = markTradeAsSuccessOrFailure(2);
				LOG.debug("Updated [" + lastChanged + "] order from processing (P) TO completed (C)");
			}
			else {
				// Mark this one as failure!!
				lastChanged = markTradeAsSuccessOrFailure(3);
				LOG.debug("Updated [" + lastChanged + "] order from processing (P) TO failed (F)");
			}
			totalChanged += lastChanged;

		} while (lastChanged > 0);

		return totalChanged;
	}
	//Need to update this as well
	/*
	 * Update a single record to success or failure
	 */
	private int markTradeAsSuccessOrFailure(int successOrFailure) {
		String code = successOrFailure == 2 ? "C" : "F";
		String sql;
		if(code.equals("C")){
			String sql_select_order_to_be_fullfilled = "SELECT id,name,stock_ticker,buy_or_sell,volume,price from " + TABLE + " WHERE " + STATUS_CODE + "='P' limit 1";
			List<Trade> tradeList  =template.query(sql_select_order_to_be_fullfilled, (resultSet, i) -> new Trade(
					resultSet.getLong(1),
					resultSet.getString(2),
					resultSet.getString(3),
					resultSet.getString(4),
					resultSet.getInt(5),
					resultSet.getInt(6)
			));

			for(Trade t : tradeList ){
				LOG.debug("Trade: " + t.toString() + " will be inserted into the db");
				String sql_create = "INSERT INTO trade (name, price, side, stock_ticker, volume) VALUES ("
                    + "'" + t.getName() + "',"
                    + t.getPrice() + ","
                    + "'" + t.getSide() + "',"
                    + "'" + t.getTicker() + "',"
                    + t.getVolume() + ");";
				template.execute(sql_create);
				try{
					this.socket.emit("trade",new JSONObject(ow.writeValueAsString(t)));
				}catch (Exception e){
					LOG.error(e.getMessage());
				}
			}

			if(tradeList.size() > 0){
				sql = "UPDATE " + TABLE + " SET " + STATUS_CODE + "='" +
						code + "' WHERE id=" + tradeList.get(0).getId() + " limit 1";
			}else{
				sql = "UPDATE " + TABLE + " SET " + STATUS_CODE + "='" +
						code + "' WHERE " + STATUS_CODE + "='P' limit 1";
			}
		}else {
			sql = "UPDATE " + TABLE + " SET " + STATUS_CODE + "='" +
					code + "' WHERE " + STATUS_CODE + "='P' limit 1";
		}


		//Check if we have captured a trade correctly
		return template.update(sql);
	}

	public void setPercentFailures(int percentFailures) {
		this.percentFailures = percentFailures;
	}

}