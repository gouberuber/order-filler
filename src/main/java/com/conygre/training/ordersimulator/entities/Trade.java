package com.conygre.training.ordersimulator.entities;



public class Trade {
    private Long id;
    private String name;
    private String ticker;
    private String side;
    private Integer volume;
    private Integer price;

    public Trade(Long id, String name, String ticker, String side, Integer volume, Integer price){
        this.id = id;
        this.name = name;
        this.ticker = ticker;
        this.side = side;
        this.volume = volume;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Trade{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", ticker='" + ticker + '\'' +
                ", side='" + side + '\'' +
                ", volume=" + volume +
                ", price=" + price +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
