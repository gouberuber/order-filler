# Dummy Order Fulfillment Simulator

### Outline
This is a simple spring-boot app that will simulate a system that sendsorders to be executed at an exchange. This system does not actually send the reuests to an exchange, it is just a SIMULATOR for testing.

On a timed interval this service will read from a MySQL database.

This system assumes the database that it reads from will have a table called "orders" which will have a column called "status_code".

You can change the above in the file OrderProcessingSimulator.java

Each trade request is expected to move through the following states:

* 0 : (intial state) All orders are initially marked with this state to indicate the request has just been created.
* 1 : (processing) When an order has been simulated as sent to an exchange it will be marked with this to indicate they are currently being handled.
* 2 : (sucess) An order that was successfull will be marked with this.
* 3 : (failed)An order that was unsuccessfull will be marked awith this.

You may edit the source code as you require to meet the needs of testing and demonstrating your system.

### Docker Image
A docker image for this project has been deployed to docker hub at: [callalyf/dummy-order-filler:0.0.1](https://hub.docker.com/r/callalyf/dummy-order-filler).

This docker image can be deployed manually to any openshift project using "Add to Project"->"Deploy Image".

You will need to point this docker image at your database with the correct table name and the correct column name for the "status_code" column. This is done by setting environment variables when deploying the image. A full list of environment variables can be found by looking in the properties files in src/main/resources. A subset of the available variables is:

If you do not include any of these environment variables then the default will be used.

| Var Name| Explanation | Default |
|-------|---------------------------|-----------------------|
|DB_HOST|Database hostname e.g. chennaidevops44.conygre.com| localhost |
|DB_USER|Database Username e.g. conygre | root|
|DB_PASS|Database Password e.g. passwordtoyourmysql|c****re|
|DB_NAME|Database Name e.g. trading_db| appDB |
|DB_TABLE|Database Table e.g. orders_table|orders|
|STATUSCODE_COL|Status code column name e.g. statusCode| status_code|